# Angular-training
Calculator

Currently I don't prevent the possibility of double input of the decimal separator.
So if you press it just one time then the calculator will display a correct result. 
However if you press this button several times then you will get an incorrect result.