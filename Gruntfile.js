module.exports = function(grunt) {

// Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: 'js/*.js',
                dest: 'build/js/<%= pkg.name %>.min.js'
            }
        },
        less: {
          development: {
            files: {
              'build/styles/styles.css': 'styles/*.less'
            }
          }
        },
        injector: {
            options: {
                template: 'index.html',
                destFile: 'build/index.html',
                ignorePath: 'build',
                addRootSlash: false
            },
            build: {
                files: {
                    'build/index.html': ['build/js/*.js', 'build/styles/*.css'],
                }
            }
        },

        jshint: {
            all: ['js/*.js']
        },
    });

// Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-injector');

// Default task(s).
    grunt.registerTask('default', ['uglify', 'less', 'injector', 'jshint']);

};