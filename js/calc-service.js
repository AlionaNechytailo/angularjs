(function () {
    // Calculate the result
    angular.module('calcApp')
        .factory('calculate', function () {
            return function () {
                return {
                    calc: function (a, b, op){
                        var res;
                        a = Number(a);
                        b = Number(b);
                        switch (op) {
                            case '+':
                                res = rounding(a + b);
                                break;

                            case '-':
                                res = rounding(a - b);
                                break;

                            case '*':
                                res = rounding(a * b);
                                break;

                            case '/':
                                res = rounding(a / b);
                                break;

                            case '%':
                                res = rounding(a % b);
                                break;
                        }
                        return res;
                    }
                }
            }
    });

    function rounding (number) {
        return Math.round(number * 100) / 100;
    }
})();