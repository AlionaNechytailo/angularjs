(function() {
    //add the calculator module to the app
    var angular, calcApp;
    angular = require('angular');
    require('calc-controller');
    require('calc-service');
    calcApp = angular.module('calcApp', ['calcModule']);
})();