(function () {
    var calcModule = angular.module('calcModule', []);

    calcModule.controller('calcCtrl', ['$scope', 'calculate', function ($scope, calculate){
        setInitialValues();
        $scope.clear = setInitialValues;
        $scope.resBtnCl = function () {
            var tmp = resultObj.calc($scope.first, $scope.second, $scope.operation);
            setInitialValues();
            $scope.results = tmp;
        }

    // Enter the num
        $scope.numBtnCl = function (num) {
            if ($scope.results !== '') {
                $scope.results = '';
            }
            if ($scope.operation === '') {
                $scope.first += num;
            } else {
                $scope.second += num;
            }
        }

    // Operation
        $scope.opertBtnCl = function (oper, curRes) {
            $scope.operation = oper;
            if (curRes !== '') {
                $scope.first = curRes;
                $scope.results = '';
            }
        }

    // Clear the calc
        function setInitialValues() {
            $scope.first = '';
            $scope.operation = '';
            $scope.second = '';
            $scope.results = 0;
        }
    }]);
})();